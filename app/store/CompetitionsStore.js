"use strict";

Ext.define('ppwr.store.CompetitionsStore', {
    extend : 'Ext.data.Store',
    requires : [
        'Ext.data.proxy.Rest',
        'ppwr.model.Competition',
    ],
    config : {
        model: 'ppwr.model.Competition',
        sorters: [{
            property: 'name',
            direction: 'DESC'
        }],
        autoLoad: true,
        proxy: {
            type:'rest',
            url:'data/competitions.js',
            reader: {
                type:'json',
                rootProperty: 'competitions'
            },
            noCache: false,
            limitParam: false,
            headers: {                
                'Accept' : 'application/json'
            }
        }
    }
});