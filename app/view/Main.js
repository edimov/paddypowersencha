"use strict";

/**
 * A simple view to present the events, using List view.
 * The list view picks data from the CompetitionsStore.
 * On click - a fullscreen panel is displayed with some event
 * info. However - this logic should be moved into controller - 
 * view and maybe a naviation grid used instead. But for the 
 * time being - this should cater for the requirements.
 *
 * @TODO Controller-view logic. Advanced sorting.
 */

var competitionInfoPanel, competitionsStore;

Ext.define('ppwr.view.Main', {
    extend : 'Ext.dataview.List',
    xtype : 'main',
    requires: [
        'Ext.TitleBar',
    ],
    fullscreen : true,
    config : {
        styleHtmlContent : true,
        store: 'CompetitionsStore',
        itemTpl : ['<span>{name}</span>'],
        disableSelection: true,
        items : [{
            docked : 'top',
            xtype : 'titlebar',
            title : 'Latest competitions',
            items : [{
                iconCls : 'arrow_down',
                iconMask : true,
                align : 'right',
                listeners: {
                    tap: function() {
                        competitionsStore.sort('name', 'ASC');
                    }
                }
            }, {
                iconCls : 'arrow_up',
                iconMask : true,
                align : 'right',
                listeners: {
                    tap: function() {
                        competitionsStore.sort('name', 'DESC');
                    }
                }
            }]
        }],
        listeners:{
            initialize: function() {
                competitionsStore = Ext.getStore('CompetitionsStore');

                competitionInfoPanel = Ext.create('Ext.Panel', {
                    centered: true,
                    hidden: true,
                    height: Ext.Viewport.getWindowHeight(),
                    width: Ext.Viewport.getWindowWidth(),
                    styleHtmlContent: true,
                    scrollable: true,
                    items: [{
                        docked: 'bottom',
                        xtype: 'toolbar',
                        items: [{
                            xtype: 'spacer'
                        },{
                            text: 'Close',
                            handler: function(){
                                competitionInfoPanel.hide();
                            }
                        }]
                    }]
                });

                Ext.Viewport.add(competitionInfoPanel);
            },
            itemtap:function(list, index, target, record, e) {
                competitionInfoPanel.setHtml(getCompetitionInfoHtml(record));
                competitionInfoPanel.show();
            },
        }
    }
});

// This is really bad - should be in proper MVC logic.
// Just simple events' data is displayed now.
var getCompetitionInfoHtml = function(rec) {
    if (typeof rec !== 'object') return '';
    if (typeof rec.data !== 'object') return '';
    if (typeof rec.data.events === 'undefined') return '';

    var res = '';

    var competitionName =   rec.data.name;
    var events =            rec.data.events;
    var eventsLength =      events.length || 0;

    res += '<h4>' + competitionName + '</h4>';
    res += '<ul>';
    for (var i = 0; i < eventsLength; i++) {
        res += '<li>';
        var rowData = [];
        rowData.push(events[i].id);
        rowData.push(events[i].name);
        rowData.push(events[i].numMarkets);
        rowData.push(events[i].startTime);
        // rowData.push(Ext.Date.format(new Date(events[i].startTime), 'Y-m-d'));

        res += rowData.join(', ');
        res += '</li>';
    }
    res += '</ul>';

    return res;
}

