"use strict";

Ext.define('ppwr.model.Event', {
    extend: 'Ext.data.Model',
    
    config: {
        fields: [
            { name: 'id', type: 'int' },
            { name: 'birSchedule', type: 'auto' },
            { name: 'competitions', type: 'auto' },
            { name: 'isDisplayed', type: 'bool' },
            { name: 'isOff', type: 'bool' },
            { name: 'liveCalendar', type: 'bool' },
            { name: 'liveNow', type: 'bool' },
            { name: 'liveOnTV', type: 'bool' },
            { name: 'markets', type: 'array' },
            { name: 'name', type: 'string' },
            { name: 'numMarkets', type: 'int' },
            { name: 'open', type: 'bool' },
            { name: 'sort', type: 'int' },
            { name: 'sports', type: 'array' },
            { name: 'startTime', type: 'int' },
            { name: 'status', type: 'bool' },
            { name: 'streamDisplayed', type: 'bool' },
            { name: 'streamEnd', type: 'auto' },
            { name: 'streamId', type: 'auto' },
            { name: 'streamProvider', type: 'auto' },
            { name: 'streamStart', type: 'auto' },
            { name: 'streamStatus', type: 'auto' },
            { name: 'teams', type: 'array' }

        ]
    }
});
