"use strict";

Ext.define('ppwr.model.Competition', {
    extend: 'Ext.data.Model',
    
    config: {
        idProperty: 'id',
        fields: [
            { name: 'id', type: 'int' },
            { name: 'name', type: 'string' },
            { name: 'sort', type: 'int' },
            { name: 'status', type: 'bool' },
            { name: 'events', type: 'array' }
        ],
        hasMany: [
            {
                model: 'ppwr.model.Event',
                autoLoad: true,
                name: 'events'
            }
        ]
    }
});
